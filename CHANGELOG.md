2.0.0 / 2019-08-12
==================

  * Generate and use docker-image to shorten CI runtime (package installation)


1.5.1 / 2019-06-20
==================

  * AsciiDoc now renders FontAwesome icons in the PDF, as per [Issue #26](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/26)


1.5.0 / 2019-06-11
==================

  * AsciiDoc now supports inline images, as per [Issue #24](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/24)

1.4.3 / 2019-05-14
==================

  * Doesn't break anymore with images inside markdown tables, as per [Issue #23](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/23)

1.4.2 / 2019-01-21
==================

  * Updated Kramdown to 2.0.0 and added kramdown-converter-pdf which is now a separate gem [Issue #20](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/20)

1.4.1 / 2018-10-08
==================

  * Now it prints the names of the files converted as suggested in [Issue #19](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/19)

1.4.0 / 2018-10-05
==================

  * Added support for subfolders for both MarkDown and AsciiDoc ([Issue #11](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/11)) and refactored a lot

1.3.6 / 2018-09-21
==================

  * Fixed self-hosted https ([Issue #9](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/9)) and apt-utils warning ([Issue #10](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/10))

1.3.5 / 2018-08-20
==================

  * Forced `en_US` and `UTF-8` in the default `ubuntu:latest` Docker image (in order to make it work in every system), as per [Issue #8](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/8)

1.3.4 / 2018-07-27
==================

  * Used default `ubuntu:latest` image as suggested in [Issue #7](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/7)

1.3.3 / 2018-07-26
==================

  * Fixed [Issue #6](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/6), which prevented the correct production of the artifacts for more than one file per markup type (basically always)

1.3.2 / 2018-07-23
==================

  * Fixed [Issue #5](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/5), now it can be run on a project with no .md, no .asciidoc, or none of them (to avoid breaking the rest of the pipeline)

1.3.1 / 2018-07-19
==================

  * AsciiDoc images (both internal and external) are now converted, in addition to the markup, as per [Issue #4](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/4).

1.3.0 / 2018-07-16
==================

  * Added support for **AsciiDoc** syntax on Wiki pages (through AsciiDoctor-PDF and various highlighter gems) as per [Issue #3](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/3) of [Milestone #1](https://gitlab.com/Andrea.Ligios/Wiki2PDF/milestones/1). Images have still problems which will be targeted in [Issue #4](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/4).

1.2.1 / 2018-07-16
==================

  * Fixed issue [#2](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/2)


1.2.0 / 2018-06-05
==================

  * It's now triggered by **Wiki modification Webhooks** and by the `Run Pipeline` button
  * It resolves external images referenced with both http and https URLs as asked in [#1](https://gitlab.com/Andrea.Ligios/Wiki2PDF/issues/1)
  * It consider only images now, not every URL:
    - `[]()`: NO
    - `![]()`: YES
  * It doesn't download the same image twice, nor it tries to create the same folder twice
  * It doesn't break with a false positive when `/upload` (or `/http`) is part of the name other than the URL, like in `!` `[foo /uploads/bar](/uploads/123/foo.png)`
  * Generated PDFs are now separated from source files and temporary files
  * URL and SCHEME for self-hosted installation have been declared as variable and put at the top, to be easily adjustable
  * Other minor improvements
  * Added splash screen in logs
  
1.0.2 / 2018-04-16
==================

  * It doesn't break anymore when put in a project which doesn't have Wiki pages yet.

